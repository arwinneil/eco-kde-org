---
date: 2024-07-02
title: "Get Started With Selenium: 4 Short Video Tutorials"
categories:  [SoK, Selenium, Sustainability, KDE Goals, KDE Eco]
author: Emmanuel Charruau and Joseph P. De Veaugh-Geiss
summary: In just 8 minutes you too will be ready to start using Selenium AT-SPI.
SPDX-License-Identifier: CC-BY-SA-4.0
authors:
- SPDX-FileCopyrightText: 2024 Emmanuel Charruau <@allon:kde.org> and Joseph P. De Veaugh-Geiss <@joseph:kde.org>
---

In just 8 minutes you too will be ready to start using Selenium AT-SPI.

Working to make your apps accessible to everybody, while reducing its power usage and improving its quality by doing more system testing, may seem as daunting as climbing Mount Everest. Luckily, KDE provides a lift to help you, Selenium AT-SPI.

Check out the following four-part guide to get started with this wonderful tool! Part 1 "An Introduction to Selenium" can be viewed here:

<p align="center"><iframe title="A Selenium Primer - Part 1: An Introduction to Selenium" src="https://tube.kockatoo.org/videos/embed/b03e88c4-6200-472b-b8bb-a9ab0099d3e0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms" width="560" height="315" frameborder="0"></iframe></p>

Click to watch [Part 2 "Setting up Selenium"](https://tube.kockatoo.org/w/p/8rDdG3c3UKk55N9VcpmaTR?playlistPosition=2&resume=true), [Part 3 "Identifying Accessibility Issues"](https://tube.kockatoo.org/w/p/8rDdG3c3UKk55N9VcpmaTR?playlistPosition=3&resume=true), and [Part 4 "Writing Selenium Tests"](https://tube.kockatoo.org/w/p/8rDdG3c3UKk55N9VcpmaTR?playlistPosition=4&resume=true).

Selenium AT-SPI, originally based on the Selenium web application for automating testing purposes, has been [ported to Qt](https://planet.kde.org/harald-sitter-2022-12-14-selenium-at-spi-gui-testing/) by Harald Sitter. With this tool, KDE can reach all three of its current goals:

 - [KDE For All](https://community.kde.org/Goals/KDE_For_All)
 - [Sustainable Software](https://community.kde.org/Goals/Sustainable_Software)
 - [Automate And Systematize Internal Processes](https://community.kde.org/Goals/Automate_and_systematize_internal_processes)

We are thankful to [Season of KDE 2024](https://eco.kde.org/blog/2024-04-11-sok24-selenium/) contributor Pradyot Ranjan for his excellent work preparing these video guides.

Let's make KDE community's software the best it can be. Have a great time using it!

#### Interested In Contributing?

Selenium AT-SPI is hosted [here](https://invent.kde.org/sdk/selenium-webdriver-at-spi). If you are interested in contributing, you can join the Matrix channels [KDE Eco](https://go.kde.org/matrix/#/#kde-eco:kde.org) and [Automation & Systematization Goal](https://go.kde.org/matrix/#/#kde-institutional-memory:kde.org) and introduce yourself. Thank you to the Season of KDE 2024 admin and mentorship team, the KDE e.V., and the incredible KDE community for supporting this project.
